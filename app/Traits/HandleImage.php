<?php

namespace App\Traits;

trait HandleImage
{
    private $imageDefault = 'default-image.png';
    private $path = 'public/images/uploads';

    public function verifyImage($request)
    {
        return $request->hasFile('image') && $request->file('image');
    }

    public function saveImage($request)
    {
        if ($this->verifyImage($request)) {
            $file = $request->file('image');
            $fileName = now().$file->getClientOriginalName();
            $file->storeAs($this->path, $fileName);
            return $fileName;
        }
        return null;
    }
}
