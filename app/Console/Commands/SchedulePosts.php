<?php

namespace App\Console\Commands;

use App\Services\PostService;
use Illuminate\Console\Command;

class SchedulePosts extends Command
{
    protected $postService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'publish post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PostService $postService)
    {
        parent::__construct();
        $this->postService = $postService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $postSchedule = $this->postService->getPostsByStatus(config('constants.status.scheduling'));

        $postSchedule->each(function ($post) {
            if($post->public_date <= now()){
                $post->status = 1;
                $post->save();
            }
        });
    }
}
