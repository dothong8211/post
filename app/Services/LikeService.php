<?php

namespace App\Services;

use App\Repositories\Like\LikeRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class LikeService
{
    private $likeRepository;

    public function __construct(LikeRepositoryInterface $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function getLikesByPost($idPost)
    {
        return $this->likeRepository->getLikesByPost($idPost);
    }

    public function storeLike($idPost)
    {
        $dataInsert = [
            'post_id' => $idPost,
            'user_id' => Auth::user()->id
        ];
        return $this->likeRepository->create($dataInsert);
    }

    public function deleteLike($id)
    {
        return $this->likeRepository->find($id)->delete();
    }
}
