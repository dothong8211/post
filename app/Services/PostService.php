<?php

namespace App\Services;

use App\Repositories\Post\PostRepositoryInterface;
use App\Traits\HandleImage;

class PostService
{
    use HandleImage;

    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function all()
    {
        return $this->postRepository->all();
    }

    public function getPostsByStatus($status)
    {
        return $this->postRepository->getPostsByStatus($status);
    }

    public function create($request)
    {
        $data = $request->all();
        if (!is_null($data['public_date']) && $data['public_date'] !== now()->toDateTimeString()) {
            $data['status'] = 0;
        }
        $data['image'] = $this->saveImage($request);
        return $this->postRepository->create($data);
    }

    public function find($id)
    {
        return $this->postRepository->find($id);
    }
}
