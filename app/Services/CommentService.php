<?php

namespace App\Services;

use App\Repositories\Comment\CommentRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class CommentService
{
    private $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function getCommentsByPost($idPost)
    {
        return $this->commentRepository->getCommentsByPost($idPost);
    }

    public function storeComment($idPost, $request)
    {
        $dataInsert = $request->all();
        $dataInsert['post_id'] = $idPost;
        $dataInsert['user_id'] = Auth::user()->id;
        return $this->commentRepository->create($dataInsert);
    }
}
