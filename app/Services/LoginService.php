<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class LoginService
{
    public function login($data)
    {
        return Auth::attempt($data);
    }

    public function checkLogin()
    {
        return Auth::check();
    }

    public function logout()
    {
        $user = Auth::user();
        $user->is_login = false;
        $user->save();
        return Auth::logout();
    }
}
