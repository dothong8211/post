<?php

namespace App\Helpers;

class Helper
{
    public static function checkImageNull($image)
    {
        if (is_null($image)) {
            return 'default-image.jpg';
        }
        return $image;
    }
}
