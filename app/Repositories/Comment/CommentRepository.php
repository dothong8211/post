<?php

namespace App\Repositories\Comment;

use App\Repositories\Base\BaseRepository;
use App\Models\Comment;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    public function getCommentsByPost($idPost)
    {
        // TODO: Implement getCommentsByPost() method.
        return $this->model->with('user')->where('post_id', $idPost)->get();
    }
}
