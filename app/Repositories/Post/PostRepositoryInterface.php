<?php

namespace App\Repositories\Post;

interface PostRepositoryInterface
{
    public function getPostsByStatus($status);
}
