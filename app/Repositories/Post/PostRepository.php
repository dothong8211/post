<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\Base\BaseRepository;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    public function getPostsByStatus($status)
    {
        return $this->model->where('status', $status)->get();
    }
}
