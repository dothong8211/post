<?php

namespace App\Repositories\Like;

use App\Models\Like;
use App\Repositories\Base\BaseRepository;


class LikeRepository extends BaseRepository implements LikeRepositoryInterface
{
    public function __construct(Like $model)
    {
        parent::__construct($model);
    }

    public function getLikesByPost($idPost)
    {
        // TODO: Implement getLikesPyPost() method.
        return $this->model->with('user')->where('post_id', $idPost)->get();
    }
}
