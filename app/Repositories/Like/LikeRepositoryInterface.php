<?php

namespace App\Repositories\Like;

interface LikeRepositoryInterface
{
    public function getLikesByPost($idPost);
}
