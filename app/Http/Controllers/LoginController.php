<?php

namespace App\Http\Controllers;

use App\Services\LoginService;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    private $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    public function getLogin()
    {
        if ($this->loginService->checkLogin()) {
            return redirect()->route('posts.index');
        }
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $data = $request->only(['email', 'password']);

        if ($this->loginService->login($data)) {
            $user = Auth::user();
            $user->is_login = true;
            $user->save();
            return redirect()->route('posts.index');
        }
        $error = new MessageBag(['error' => 'Wrong account or password!!']);
        return redirect()->back()->withInput($data)->withErrors($error);
    }

    public function logout()
    {
        $this->loginService->logout();
        return redirect()->route('login');
    }
}
