<?php

namespace App\Http\Controllers;

use App\Http\Resources\LikeCollection;
use App\Models\Like;
use App\Services\LikeService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LikeController extends Controller
{
    protected $likeService;

    public function __construct(LikeService $likeService)
    {
        $this->likeService = $likeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idPost)
    {
        $likes = $this->likeService->getLikesByPost($idPost);
        return response()->json($likes, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($idPost)
    {
        $like = $this->likeService->storeLike($idPost);
        return response()->json(['resource' => $like, 'status' => Response::HTTP_OK, 'like' => true], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->likeService->deleteLike($id);
        return response()->json(['message' => 'unlike', 'status' => Response::HTTP_OK, 'like' => false], Response::HTTP_OK);
    }
}
