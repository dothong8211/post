<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function change(Request $request)
    {
        session()->put('website_language', $request->lang);
        return redirect()->back();
    }
}
