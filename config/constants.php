<?php
return [
    'status' => [
        'publish' => 1,
        'scheduling' => 0
    ],
    'lang' => [
        'en' => 'en',
        'vi' => 'vi'
    ]
];
