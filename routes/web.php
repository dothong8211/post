<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\LanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth', 'check.logout', 'set.locale'])->group(function () {
    Route::resource('posts', PostController::class)->except([
        'edit',
        'update',
        'destroy'
    ]);

    Route::resource('posts.comments', CommentController::class)->only([
        'index',
        'store'
    ])->shallow();

    Route::resource('posts.likes', LikeController::class)->only([
        'index',
        'store',
        'destroy'
    ])->shallow();

    Route::get('/language', [LanguageController::class, 'change'])->name('change-language');
});

Route::get('/login', [LoginController::class, 'getLogin'])->name('login');
Route::post('/login', [LoginController::class, 'postLogin'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
