<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           [
               'id' => 1,
               'email' => 'dothong@gmail.com',
               'name' => 'dothong',
               'password' => Hash::make('123456'),
               'is_login' => false,
               'created_at' => now(),
               'updated_at' => now()
           ],
            [
                'id' => 2,
                'email' => 'admin@gmail.com',
                'name' => 'admin',
                'password' => Hash::make('123456'),
                'is_login' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 3,
                'email' => 'user@gmail.com',
                'name' => 'user',
                'password' => Hash::make('123456'),
                'is_login' => false,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
