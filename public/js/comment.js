(function ($, window, document) {
    $(function () {

        $('#btn-comment-now').on('click', function () {
            $('#content').focus();
        })

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        function callApi(url, method, data = {}) {
            return $.ajax({
                url: url,
                type: method,
                data: data
            })
        }

        //get list comments
        function getListComments() {
            let url = $('#route-get-list-comments').data('url');
            callApi(url, 'GET')
                .done(function (response) {
                    renderListComment(response);
                });
        }

        getListComments();

        function renderListComment(comments) {
            if (comments.length) {
                let html = comments.map(comment =>
                    `<p class="card-text pl-2"> <strong>${comment.user.name}: </strong>${comment.content}</p>`
                );
                $('#list-comments').html(html);
            } else {
                $('#list-comments').html(`<p style="font-size: 13px; color: gray">No Comments</p>`);
            }
            //count comment
            $('#count-comments').html(`comments: ${comments.length}`);
        }

        //store comment
        $('#form-comment').on('submit', function (e) {
            e.preventDefault();
            let dataInsert = {};
            let url = $('#route-store-comment').data('url');
            $(this).serializeArray().forEach(data => dataInsert[data.name] = data.value);

            callApi(url, 'POST', dataInsert)
                .done(function (response) {
                    $('#content').val('');
                    getListComments();
                    removeErrors();
                })
                .fail(function (response) {
                    showErrors(response);
                });

        });

        function showErrors(response) {
            $.each(response.responseJSON.errors, function (name, message) {
                $('#error-' + name).text(message[0]);
            })
        }

        function removeErrors() {
            $('#error-content').text('');
        }

        //get list like
        function getListLikes() {
            let url = $('#route-get-list-likes').data('url');
            callApi(url, 'GET')
                .done(function (response) {
                    handleLike(response);
                });
        }

        getListLikes();

        function handleLike(likes) {
            let userName = $('#btn-like').attr('username');
            let btnLike = $('#btn-like');
            $.each(likes, function (index, like) {
                if (like.user.name == userName) {
                    btnLike.css('color', '#0f6674');
                    btnLike.attr('status', '1');
                    btnLike.attr('id-like', like.id)
                    return;
                }
            });
            //count like
            $('#count-likes > span').text(`${likes.length}`);
        }

        $('#btn-like').on('click', function () {
            let status = $('#btn-like').attr('status');
            let idLike = $(this).attr('id-like');
            let url = status == 0 ? $('#route-store-like').data('url') : $('#route-destroy-like').data('url') + '/' + idLike;
            let method = status == 0 ? 'POST' : 'DELETE';

            callApi(url, method)
                .done(function (response) {
                    console.log(response);
                    getListLikes();
                    resetLike(response);
                });
        });

        function resetLike(response) {
            if(!response.like)
                $('#btn-like').css('color', '')
                $('#btn-like').attr('status', '0');
                $('#btn-like').removeAttr('id-like');
        }

        //pusher
        let pusher = new Pusher('2c997a66160f16754069', {
            cluster: 'ap1'
        })

        let channel  = pusher.subscribe('push-notification');
        channel.bind('notification', function (data){
            $('.wrapper-notification').html(`<a class="dropdown-item" href="#">${data.message}</a>`);
            $('.btn-notification > i').css('color', 'red');
        });
        //end pusher

        $('.btn-notification').on('click', function (){
            $('.btn-notification > i').css('color', 'unset');
        });
    });
}(window.jQuery, window, document))
