(function ($, window, document) {
    $(function () {
        //preview image
        $('#file-image').on('change', function (e) {;
            let reader = new FileReader();
            reader.onload = function (e) {
                $('#preview-image').attr('src', e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
}(window.jQuery, window, document))

