@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div class="mt-4 mb-4">
            <a href="{{ route('posts.index') }}">back</a>
        </div>
        <div class="card" >
            <img class="card-img-top" src="{{ asset('storage/images/uploads/'.\App\Helpers\Helper::checkImageNull($post->image)) }}" alt="Card image cap" >
            <div class="card-body">
                <h5 class="card-title">{{ $post->title }}</h5>
                <p class="card-text">{{ $post->content }}</p>
                <p id="count-comments"></p>
                <p id="count-likes">
                    <i class="far fa-thumbs-up"></i> <span></span>
                </p>
                <div class="d-flex flex-row">
                    <span style="cursor:pointer; margin-right: 10px" id="btn-comment-now">
                        <i class="far fa-comments"></i> comment now
                    </span>
                    <span style="cursor:pointer; margin-right: 10px" id="btn-like" username="{{ auth()->user()->name }}" status="0">
                        <i class="far fa-thumbs-up"></i> like
                    </span>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-body">
                <h5 class="card-title">All comments</h5>
                <div id="list-comments"></div>
                <div style="margin: 20px 0px; height: 1px; background: black"></div>
                <form id="form-comment">
                    <div class="form-group" style="display: flex">
                        <input type="text" class="form-control" id="content" placeholder="Enter your comment" name="content" autocomplete="off">
                        <button class="btn btn-primary">send</button>
                    </div>
                    <small class="form-text text-muted" style="color: red !important;" id="error-content"></small>
                </form>
            </div>
        </div>
    </div>

    {{--route--}}
    <input type="hidden" id="route-get-list-comments" data-url="{{ route('posts.comments.index', $post->id) }}">
    <input type="hidden" id="route-store-comment" data-url="{{ route('posts.comments.store', $post->id) }}">
    <input type="hidden" id="route-get-list-likes" data-url="{{ route('posts.likes.index', $post->id) }}">
    <input type="hidden" id="route-store-like" data-url="{{ route('posts.likes.store', $post->id) }}">
    <input type="hidden" id="route-destroy-like" data-url="{{ url('/likes') }}">
@endsection

@push('script')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="{{ asset('js/comment.js') }}"></script>
@endpush
