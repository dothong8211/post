@extends('layouts.app')
@section('content')
    <div class="container">
        @if(\Session::has('message'))
            <div class="mt-4">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ \Session::get('message') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <h1 class="text-center mt-5">{{ trans('label.list_post') }}</h1>
        <div class="btn-add-post text-right mt-4 mb-4">
            <a href="{{ route('posts.create') }}" class="btn btn-sm btn-primary">{{ trans('label.add_post') }}</a>
        </div>
        <div class="row">
            @foreach($posts as $post)
                <div class="col-xl-3">
                    <div class="card" style="width: 100%">
                        <img class="card-img-top"
                             src="{{ asset('storage/images/uploads/'.\App\Helpers\Helper::checkImageNull($post->image)) }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p class="card-text">{{ $post->content }}</p>
                            <a href="{{ route('posts.show', $post->id) }}" class="btn btn-primary btn-sm">{{ trans('label.view_detail') }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@push('script')

@endpush
