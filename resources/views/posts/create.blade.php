@extends('layouts.app')
@push('css')
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
@endpush
@section('content')
    <div class="container mb-4">
        <h1 class="text-center mt-5">{{ trans('label.create_post') }}</h1>
        <div class="btn-back-index mt-3 mb-3">
            <a href="{{ route('posts.index') }}">
                <i class="fas fa-backward" ></i>
                {{ trans('label.back') }}
            </a>
        </div>
        <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">{{ trans('label.title') }}</label>
                <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
                @error('title')
                <small class="form-text text-muted" style="color: red !important;">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">{{ trans('label.content') }}</label>
                <input type="text" class="form-control" id="content" placeholder="Enter content" name="content">
                @error('content')
                <small class="form-text text-muted" style="color: red !important;">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <div class="wrapper-image">
                    <label>{{ trans('label.image') }}</label>
                    <div class="box-image">
                        <img src="{{ asset('images/default-image.jpg') }}" alt="" width="150px" height="150px" id="preview-image">
                    </div>
                    <div class="btn btn-sm btn-success btn-image">
                        {{ trans('label.choose_image') }} <i class="fas fa-upload"></i>
                        <input type="file" id="file-image" name="image">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label style="display: block">{{ trans('label.public_date') }}</label>
                <input type="datetime-local" name="public_date">
            </div>
            <button type="submit" class="btn btn-primary">{{ trans('label.create') }}</button>
        </form>
    </div>
@endsection
@push('script')
    <script src="{{ asset('js/post.js') }}"></script>
@endpush
