<?php
return [
    'add_post' => 'thêm mới',
    'view_detail' => 'chi tiết',
    'log_out' => 'đăng xuất',
    'list_post' => 'Danh sách',
    'back' => 'Quay lại',
    'title' => 'Tiêu đề',
    'content' => 'Nội dung',
    'image' => 'Ảnh',
    'choose_image' => 'Tải ảnh',
    'public_date' => 'Ngày đăng bài',
    'create' => 'Tạo mới',
    'create_post' => 'Thêm bài viết',
    'hello' => 'Xin chào'
];
