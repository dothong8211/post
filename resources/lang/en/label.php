<?php
return [
    'add_post' => 'add post',
    'view_detail' => 'View detail',
    'log_out' => ' log out',
    'list_post' => 'List post',
    'back' => 'back',
    'title' => 'Title',
    'content' => 'Content',
    'image' => 'Image',
    'choose_image' => 'Choose image',
    'public_date' => 'Public date',
    'create' => 'Create',
    'create_post' => 'Create post',
    'hello' => 'hello'
];
